#!/usr/bin/php
<?php
# checkAzureBackups.php
# Pulls backup info from updateomatic DB for specified node. Compares it against Azure backup schedules. Returns state based on last ran and run success.
# usage: checkAzureBackups.php -n <node name>
# 2018 Dan Ialacci

require_once '/var/ansible/updateomatic/lib/meekrodb.2.3.class.php';
DB::$user = 'updateomatic';
DB::$password = '';
DB::$dbName = 'updateomatic_dev';
DB::$host = 'dbhost';

define('STATE_OK', 0);
define('STATE_WARNING', 1);
define('STATE_CRITICAL', 2);
define('STATE_UNKNOWN', 3);

$currentDateTime = date('Y-m-d H:i:s');

$options = getopt("n:");

//extract hostname from FQDN, if inpu
$parts = explode(".",$options['n']);
$nodeName = $parts[0];


// Azure Subscription Info
$subscriptionID = "";
$directoryID = "";
$appID = "";
$appSecret = "";
$resourceGroup = "";
$vaultName = "";
$apiVersion = "2017-07-01";

$tokenURI = "https://login.microsoftonline.com/$directoryID/oauth2/token";
$tokenData = "grant_type=client_credentials&client_id=$appID&client_secret=$appSecret&resource=https%3A%2F%2Fmanagement.azure.com%2F";

$bkupRESTURI = "https://management.azure.com/Subscriptions/$subscriptionID/resourceGroups/$resourceGroup/providers/Microsoft.RecoveryServices/vaults/$vaultName/backupPolicies?api-version=$apiVersion";

// fetch auth token
$ct = curl_init($tokenURI);
curl_setopt_array($ct, array(
    CURLOPT_POSTFIELDS => $tokenData,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_VERBOSE => 0

));
$out = curl_exec($ct);
curl_close($ct);

$result = json_decode($out,TRUE);
$token = $result['access_token'];

// use token to query API
$ct = curl_init($bkupRESTURI);
curl_setopt_array($ct, array(
    CURLOPT_HTTPHEADER  => array('Authorization: Bearer ' . $token, 'Content-Type: application/json'),
    CURLOPT_RETURNTRANSFER  =>true,
    CURLOPT_VERBOSE => 0
));
$out = curl_exec($ct);
curl_close($ct);

$azBkupItems  = json_decode($out, TRUE);

// fix wonky Azure date format, drop microseconds.
function mysqlDate($input,$format) {
    $inputFormat = '!Y-m-d\TH:i:s+';
    
    switch ($format) {
        case "date" :
            $outputFormat = "Y-m-d";
            break;
        case "time" :
            $outputFormat = "H:i:s";
            break;
        case "dateTime":
            $outputFormat = 'Y-m-d H:i:s';
            break;
    }

    if ($objTime = date_create_from_format($inputFormat, $input)) {
        $output = date_format($objTime , $outputFormat);
        return $output;
    }else{
      #  var_dump(date_get_last_errors());
        return "false";
    }
}
// parse api results and build array with info for each job. 
$i = 0;
$bkupJobs = array();

foreach ($azBkupItems['value'] as $item) {
    
    if(isset($item['properties']['schedulePolicy'])) {
        $scheduleRunFrequency = $item['properties']['schedulePolicy']['scheduleRunFrequency'];
        $scheduleRunTimes = $item['properties']['schedulePolicy']['scheduleRunTimes'][0];

    } else {
        $scheduleRunFrequency = null;
        $scheduleRunTimes = null;
    }

    $bkupJobs[$i]['name'] = $item['name'];
    $bkupJobs[$i]['type'] = $item['properties']['backupManagementType'];
    $bkupJobs[$i]['frequency'] = $scheduleRunFrequency;
    $bkupJobs[$i]['time'] = mysqlDate($scheduleRunTimes,"time");
    $bkupJobs[$i]['configDate'] = mysqlDate($scheduleRunTimes,"date");
 
    $i++;
}

$query = "SELECT id,protectionStatus,protectionState,lastBackupStatus,lastBackupTime,lastRecoveryPoint,policyName,tblUpdateTime FROM backups WHERE backups.id IN (select id from nodes where nodes.name = %s)";

if (!($nodeBkupStatus = DB::query($query,$nodeName))) {
echo "WARN: Node $nodeName not found in DB.";
exit(STATE_WARNING);

}




//var_dump($nodeBkupStatus);

// search $bkupJobs and find wich job matches the specified node. 

$jobIndex = array_search($nodeBkupStatus[0]['policyName'], array_column($bkupJobs,'name'));
$jobRunTime =  $bkupJobs[$jobIndex]['time'];
$jobRunFreq =  $bkupJobs[$jobIndex]['frequency'];

$nodeLastRunTime = $nodeBkupStatus[0]['tblUpdateTime'];
$nodeLastBackupStatus = $nodeBkupStatus[0]['lastBackupStatus'];
$nodeLastBackupCompleteTime = $nodeBkupStatus[0]['lastBackupTime'];
$nodeLastProtectionStatus = $nodeBkupStatus[0]['protectionStatus'];

$delta = strtotime($currentDateTime) - strtotime($nodeLastRunTime);
$delta = $delta / 3600;

$tst = $currentDateTime;

# check if returned lastRunTime is > 24 hours from now
if (floor($delta) > 23) {
    echo "longer than 24 hours ($tst) since backup DB update. Last update for $nodeName was $nodeLastRunTime ";
     exit(STATE_WARNING);
}else{
    #check lastBackupTime, if > than 24 hours from policy Time return warning with last backup time and lastBackupStatus
        
        # if it's a daily job, assume the timestamp is today, append todays date to time pulled from job schedule. find delta between that and <now>
        if ($jobRunFreq == "Daily") {
        
            #clean up this obj -> string -> obj mess
            $jobLastRunTime = date_format(date_create_from_format("G:i:s", $bkupJobs[$jobIndex]['time']),'Y-m-d H:i:s');
            $delta = strtotime($currentDateTime) - strtotime($jobLastRunTime);
        }

    # if time since last backup is more than 23 hours, return error
    if (floor($delta / 3600) > 23) {
        echo "longer than 24 hours since last backup!. Last backup for $nodeName was $jobLastRunTime ";
        exit(STATE_WARNING);
    }else{
        # if all the times are correct, check the status. If completed AND healthy, return OK
        if ($nodeLastBackupStatus == "Completed" && $nodeLastProtectionStatus == "Healthy") {
           echo "OK: Backup for " . $nodeName  . " ran from $nodeLastRunTime to $nodeLastBackupCompleteTime and finished with status $nodeLastBackupStatus";
           exit(STATE_OK);
        }elseif($nodeLastBackupStatus == "Completed" && $nodeLastProtectionStatus != "Healthy") {
            echo "WARN: Backup for " . $nodeName  . " ran at $nodeLastRunTime with status $nodeLastBackupStatus but failed health check!";
            exit(STATE_WARNING);
        }else{
            echo "CRIT: Backup for " . $nodeName  . " ran at $nodeLastRunTime with status $nodeLastBackupStatus";
            exit(STATE_CRITICAL);
        }

    }

}
